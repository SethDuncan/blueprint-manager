<?php
/*
Template Name: Contact Template
*/
get_header(); ?>

<div class="breadcrumbs"><a href="#">Home</a> <i class="icon-double-angle-right grey"></i> Contact Us</div>
	
	<div class="inner_content">
		<h1 class="title">Contact Us</h1>	
		
		<!--<h1>It's art if it can't be explained. It's <span>fashion</span> if no one asks for an <span>explanation.</span> 
		It's <span class="hue">design</span> if it doesn't need explanation. - Wouter Stokkel</h1>-->
		
		<div class="row">
		
		
			<div class="span4 pad15">
				<h3 class="title-divider span4">Get In<strong> Touch</strong><span></span></h3>
				<p>Looking for a custom bluprint quote, have a question or comment? Please feel free to send us an email or fill in our handy contact form. We aim to reply within 48 hours.</p>

				<address>
					<span class="black">email</span><br>
					<a href="mailto:admin@xenmaster.com">admin@xenmaster.com</a><br>
					<span class="black">phone</span><br>
					(123) 456 7890
				</address>
		
				<a href="#" class="zocial icon twitter"></a>
			
				<a href="#" class="zocial icon facebook"></a>
				
				<a href="#" class="zocial icon googleplus"></a>
				
				<a href="#" class="zocial icon linkedin"></a>
				
				<a href="#" class="zocial icon dribbble"></a>
			</div>
		
			<div class="span8 pad15">
				<div class="contact_form well">  
					<div id="note"></div>
					<div id="fields">
						<form id="ajax-contact-form">
							<p class="form_info">name <span class="required">*</span></p>
							<input class="span5" type="text" name="name" value="" />
							<p class="form_info">email <span class="required">*</span></p>
							<input class="span5" type="text" name="email" value=""  />
							<p class="form_info">subject</p>
							<input class="span5" type="text" name="subject" value="" /><br>
							<p class="form_info">message</p>
							<textarea name="message" id="message" class="span7" ></textarea>
							<div class="clear"></div>
							
							<input type="submit" class="btn btn-large btn-inverse btn-form" value="submit" />
							<input type="reset"  class="btn btn-large btn-inverse btn-form" value="reset" />
							<div class="clear"></div>
						</form>
					</div>
				</div>                   
			</div>                	
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){	
		$("#ajax-contact-form").submit(function() {
			var str = $(this).serialize();		
			$.ajax({
				type: "POST",
				url: "contact_form/contact_process.php",
				data: str,
				success: function(msg) {
					// Message Sent - Show the 'Thank You' message and hide the form
					if(msg == 'OK') {
						result = '<div class="notification_ok">Your message has been sent. Thank you!</div>';
						$("#fields").hide();
					} else {
						result = msg;
					}
					$('#note').html(result);
				}
			});
			return false;
		});															
	});		
</script>				
<?php get_footer(); ?>