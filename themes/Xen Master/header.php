<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width">
		<title><?php wp_title(); ?></title>
		<link href='http://fonts.googleapis.com/css?family=Arvo:400|Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
		<!--[if IE]>
			<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
			<link href="http://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css">
			<link href="http://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet" type="text/css">
			<link href="http://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css">
		<![endif]-->
		<link href="<?php bloginfo('template_url') ?>/styles/bootstrap.css" rel="stylesheet">
		<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">
		<link href="<?php bloginfo('template_url') ?>/styles/blue-theme.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/styles/rateit.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/styles/jquery.dataTables.css" media="screen" />
		<link href="<?php bloginfo('stylesheet_url')?>" rel="stylesheet">
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if IE 7]>
		<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/styles/font-awesome-ie7.min.css">
		<![endif]-->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<?php wp_head(); ?>
	</head>

	<body>
<!--header-->
	<div class="header">
		<!--logo-->
			<div class="container">
					<div class="logo">
						 <a href="<?php echo get_settings('home'); ?>"><img src="<?php bloginfo("template_url"); ?>/img/Logo.png" alt="Zen Master IT BluePrints"/></a>  
					</div>
					<!--menu-->
					<nav id="main_menu">
					<div class="menu_wrap">
						<ul class="nav sf-menu">
							
							<?php echo xen_navigation_menu(); ?>
						
							<!--<li class="active"><a href="index.html"><i class="icon-home  darkgrey"></i><br>Home</a> </li>
							<li class="sub-menu"><a href="javascript:{}"> <i class="icon-book darkgrey"></i><br>Diagrams</a>
							<ul>
								<li><a href="#"><span>--</span>Type 1</a></li>
								<li><a href="#"><span>--</span>Type 2</a></li>
								<li><a href="#"><span>--</span>Type 3</a></li>
								<li><a href="#"><span>--</span>Type 4</a></li>
								<li><a href="#"><span>--</span>Request Diagram</a></li>
								<li><a href="#"><span>--</span>Submit Diagram</a></li>
							</ul>
							</li>
							<li class="sub-menu"><a href="javascript:{}"><i class="icon-bullhorn darkgrey"></i><br>Blog</a>
							<ul>
								<li><a href="#"><span>--</span>Blog</a></li>
								<li><a href="#"><span>--</span>Blog Post</a></li>
								<li><a href="#"><span>--</span>Blog Variation</a></li>
								<li><a href="#"><span>--</span>Post Variation</a></li>
							</ul>
							</li>
							<li><a href="#"><i class="icon-pencil darkgrey"></i><br>Contact</a></li>
							<li class="last"><a href="#"><i class="icon-key darkgrey"></i><br>Login</a></li> -->
						</ul>
					</div>
				</nav>
			</div>
		</div>
	<!--//header-->
	<!--page-->
	<div class="container content wrapper">