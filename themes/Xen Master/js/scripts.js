function saveRating(){

	

	var rating = $(".rateit").rateit("value");
	var bpID = $(".rateit").attr("data-id");
	
	$(".rateit").rateit('readonly', true);
	
	var rateSave = 
	{
		action: "rateBluePrint", rating: rating, nonce: ajNonce, blueprintID: bpID
	};
	
	
	
	
	$.ajax({
		type: "POST",
		url: ajURL ,
		data: rateSave,
		dataType: 'json',
		success: function(data) {
			if (data.response == 'failed'){
			}
			else{
				
			}
			
		}
	});
	
	
}


$(document).ready(function(){	



/***************************************************
	MENU
***************************************************/
	$("<select />").appendTo("nav#main_menu div");
	
	// Create default option "Go to..."
	$("<option />", {
	   "selected": "selected",
	   "value"   : "",
	   "text"    : "choose a page"
	}).appendTo("nav#main_menu select");	
	
	// Populate dropdowns with the first menu items
	$("nav#main_menu li a").each(function() {
	 	var el = $(this);
	 	$("<option />", {
	     	"value"   : el.attr("href"),
	    	"text"    : el.text()
	 	}).appendTo("nav#main_menu select");
	});
	
/***************************************************
	RESPONSIVE MENU
***************************************************/		
  	$("nav#main_menu select").change(function() {
    	window.location = $(this).find("option:selected").val();
  	});
	
/***************************************************
		TOOLTIP & POPOVER
***************************************************/
$("[rel=tooltip]").tooltip();
$("[data-rel=tooltip]").tooltip();

/***************************************************
		CAROUSEL - STOP AUTO CYCLE
***************************************************/
 $('.carousel').carousel({
    interval: false});

/***************************************************
		PRETTYPHOTO
***************************************************/
	$("a[rel^='prettyPhoto']").prettyPhoto();
	jQuery("a[rel^='prettyPhoto'], a[rel^='lightbox']").prettyPhoto({
overlay_gallery: false, social_tools: false,  deeplinking: false
});
	
/***************************************************
		HOVER
***************************************************/
	$(".hover_img").live('mouseover',function(){
			var info=$(this).find("img");
			info.stop().animate({opacity:0.2},500);
		}
	);
	$(".hover_img").live('mouseout',function(){
			var info=$(this).find("img");
			info.stop().animate({opacity:1},800);
		}
	);
	
/***************************************************
	LOGO FADE DOWN EFFECT
***************************************************/
	 	          $(function() { 	
        		$('.logo').show().addClass("animated hinge fadeInDown");
	        });  
	
/***************************************************
		BACK TO TOP LINK
***************************************************/
			$(window).scroll(function() {
				if ($(this).scrollTop() > 200) {
					$('.go-top').fadeIn(200);
				} else {
					$('.go-top').fadeOut(200);
				}
			});
			
			// Animate the scroll to top
			$('.go-top').click(function(event) {
				event.preventDefault();
				
				$('html, body').animate({scrollTop: 0}, 300);
			})
			
		});	

/***************************************************
	IFRAME
***************************************************/
	$("iframe").each(function(){
		var ifr_source = $(this).attr('src');
		var wmode = "wmode=transparent";
		if(ifr_source.indexOf('?') != -1) {
		var getQString = ifr_source.split('?');
		var oldString = getQString[1];
		var newString = getQString[0];
		$(this).attr('src',newString+'?'+wmode+'&'+oldString);
		}
		else $(this).attr('src',ifr_source+'?'+wmode);
	});
	
/***************************************************
	VALIDATE PRETTY PHOTO
***************************************************/
$('a[data-rel]').each(function() {
$(this).attr('rel', $(this).attr('data-rel')).removeAttr('data-rel');
});


/*****************************************************
	Rating Stars
*******************************************************/
	$(".rateit").rateit({ 
		ispreset: true,
		max: 5,
		resetable : false
	});
	
	$(".rateit").bind('rated', function () { saveRating(); } );

/***************************************************
	Data Tables
****************************************************/
	
	$(".bpListTable").dataTable({
		"bLengthChange" : false
	});
	

/*************************************************
	Jump to Comments Form
*************************************************/

	$("a.jump-comment-form-btn").click(function (e) {
	
		e.preventDefault();
	
		$("html, body").animate({scrollTop: $("#add-comment-section").offset().top}, 'slow');
		
		
	});