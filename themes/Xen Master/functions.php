<?php
/**
 * Xen Master Customer Functions 
*/




	add_filter( 'show_admin_bar', 'admin_bar_visibility' );
	
	remove_filter( 'the_content', 'wpautop' );

	function custom_login_logo() {
		echo '<style type="text/css">
			.login h1 a { 
				background-image: url("'.get_bloginfo('template_directory').'/img/Login-Logo.png") !important; 
				width: 226px;
				height: 33px;
				background-size: 226px 33px; 
				margin: 0px auto;
				}
			input#wp-submit {
				background: #11ACEE;
			}
			.login #nav a, .login #backtoblog a {
				color: #11ACEE !important;
			}
			.login #nav a:hover, .login #backtoblog a:hover {
				color: #7b7b7b !important;
			}
		</style>';
}
	add_action('login_head', 'custom_login_logo');


	function custom_login_logo_url () {
	
		return get_bloginfo('url');
	
	}
	
	add_filter("login_headerurl","custom_login_logo_url");
	
	function custom_login_title() {
		return get_option( 'blogname' );
	}
	add_filter( 'login_headertitle', 'custom_login_title' );
	
	
	function admin_bar_visibility(){


		if (is_user_logged_in()){
			return false;
			//return true; Uncomment and delete line above
		}
		else{
			return false;
		}


	}
	
	
	function getCurrentPageID(){
	
		global $post;
		
		
		return $post->ID;
	}
	
	
	
	function xen_navigation_menu() {
	
		/// Nav Icon Dictionary
		$navDict = array("home" => "icon-home", "blueprints" => "icon-book", "blog" => "icon-bullhorn", "contact" => "icon-pencil", "login" => "icon-key");
		$currentPage = getCurrentPageID();
		$navpages = get_pages( array('sort_column' => 'menu_order', 'sort_order' => 'asc', 'parent' => 0, 'hierarchical' => false) );
		$nav_menu_string = '';
		
		foreach($navpages as $page){
		
			if ($page->ID == getCurrentPageID()){
				if (strtolower($page->post_title) == "blueprints"){
					$nav_menu_string .= '<li class="sub-menu active"><a href="'. get_page_link( $page->ID ) .'"><i class="'. $navDict[strtolower($page->post_title)]. ' darkgrey"></i><br>'. $page->post_title .'</a>';
					$nav_menu_string .= xen_blueprint_submenu($page->ID);
					$nav_menu_string .= '</li>';
				}
				else{
					$nav_menu_string .= '<li class="active"><a href="'. get_page_link( $page->ID ) .'"><i class="'. $navDict[strtolower($page->post_title)] .' darkgrey"></i><br>'. $page->post_title .'</a></li>';
				}
			}
			else
			{
				if (strtolower($page->post_title) == "blueprints"){
					$nav_menu_string .= '<li class="sub-menu"><a href="'. get_page_link( $page->ID ) .'"><i class="'. $navDict[strtolower($page->post_title)]. ' darkgrey"></i><br>'. $page->post_title .'</a>';
					$nav_menu_string .= xen_blueprint_submenu($page->ID);
					$nav_menu_string .= '</li>';
				}
				else {
					$nav_menu_string .= '<li><a href="'. get_page_link( $page->ID ) .'"><i class="'. $navDict[strtolower($page->post_title)] .' darkgrey"></i><br>'. $page->post_title .'</a></li>';
				}
			}
			
		}
		
		///// Add Login Menu Item if Not Logged In
		if( !is_user_logged_in()){
			$nav_menu_string .= '<li class="last"><a href="'. wp_login_url() .' "><i class="'. $navDict["login"] .' darkgrey"></i><br>Login</a></li>';
		}
	
		return $nav_menu_string;
	
	}
	
	function xen_blueprint_submenu($menuID) {
	
		
		$subMenuStr .= '<ul>';
		
		$bpChildPages = get_pages(array('sort_column' => 'menu_order', 'sort_order' => 'asc', "child_of" => $menuID));
		
		foreach($bpChildPages as $cpage)
		{
			$subMenuStr .= '<li><a href="'. get_page_link($cpage->ID) .'"><span>--</span>'. $cpage->post_title .'</a></li>';
		}
		
		$subMenuStr .= '</ul>';
		
		return $subMenuStr;
	}
	
	add_shortcode('recentBlueprintCTA', 'getRecentBlueprint');
	
	
	function getRecentBlueprint() {
	
		$bManager = new BlueprintManager();
	
		$bpRecent = $bManager->selectRecentBlueprint();
	
		$recentStr = '<h3>Recent Addition</h3><strong>' . $bpRecent[0]->Description .'</strong><br/>Architect: ' . $bpRecent[0]->Architect . '<br/>Added: '. date('F jS Y', strtotime($bpRecent[0]->DateSubmitted)). '<br/><br/><a class="btn btn-primary btn-medium" href="' . wp_nonce_url(get_permalink($bpRecent[0]->Post_FK)) . '">view</a><br/>';
	
		return $recentStr;
	
	}
	
	function getRecentBluePrintFooter() {
	
		$bManager = new BlueprintManager();
		
		$bpRecent = $bManager->selectRecentBlueprint();
		
		if(!empty($bpRecent)){
		
		$recURL = wp_nonce_url(get_permalink($bpRecent[0]->Post_FK));
		
		$recentBPStr .= '<div id="recentBlueprint"><a href="'. $recURL .'"><img src="' . $bManager->upload_dir . $bpRecent[0]->Blueprint .'" /></a></div>
						<h5>
							<a href="'. $recURL .'">' . $bpRecent[0]->Description . '</a>
						</h5>
						<p>
							'. $bpRecent[0]->Notes .'
						</p>
						<p>
							<a href="'. $recURL .'" class="more-link">View Blue Print &rarr;</a>
						</p>';
		}
		return $recentBPStr;
	
	}
	
	function get_client_ip() {
		 $ipaddress = '';
		 if (getenv('HTTP_CLIENT_IP'))
			 $ipaddress = getenv('HTTP_CLIENT_IP');
		 else if(getenv('HTTP_X_FORWARDED_FOR'))
			 $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		 else if(getenv('HTTP_X_FORWARDED'))
			 $ipaddress = getenv('HTTP_X_FORWARDED');
		 else if(getenv('HTTP_FORWARDED_FOR'))
			 $ipaddress = getenv('HTTP_FORWARDED_FOR');
		 else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
		 else if(getenv('REMOTE_ADDR'))
			 $ipaddress = getenv('REMOTE_ADDR');
		 else
			 $ipaddress = 'UNKNOWN';

		 return $ipaddress; 
	}
	
	
	
	add_action('wp_ajax_rateBluePrint', 'rateBluePrint');

	function rateBluePrint() {

		$nonce = $_POST['nonce'];
		$rating = $_POST['rating'];
		$bpID = $_POST['blueprintID'];
		
		$cIP = get_client_ip();
		
		if (wp_verify_nonce( $nonce, 'ajax_referrer_nonce'))
		{
			$bpManager = new BlueprintManager();
			header("Content-type: application/json");
			
			$bpManager->insertRating($bpID, $rating, $cIP);
			
			
			echo json_encode(
				array('response' => 'success')
			);
			
			
		}
		else
		{
		
			header("Content-type: application/json");
			
			echo json_encode(
				array('response' => 'failed')
			);
		
		}

		die();
	}

	add_action('wp_ajax_rateBluePrint', 'rateBluePrint');
	add_action('wp_ajax_nopriv_rateBluePrint', 'rateBluePrint');

	function get_popular_posts() {
	
		$bManager = new BlueprintManager();
		
		$results = $bManager->selectBluePrintsTopRated(3);
		
		foreach($results as $result){
		
			echo '<li class="media"><div class="cat-icon pull-left"><i class="' . get_post_category_icon(strtolower($result->CategoryDescription)) . '"></i></div><div class="media-body"><a href="'. wp_nonce_url(get_permalink($result->Post_FK), "phbm-nonce") .'">' . $result->Description.'</a><br/><i class="icon-calendar hue"></i>&nbsp;' . date('F jS Y', strtotime($result->DateSubmitted)) . '</div></li>';
		
		}
		
	}
	
	
	function get_post_category_icon($catName) {
	
		$iconDict = array('high availability' => 'icon-globe', 'load balancing' => 'icon-exchange', 'network' => 'icon-sitemap', 'storage' => 'icon-hdd', 'virtualization' => 'icon-laptop', 'backups & dr' => 'icon-time', 'security' => 'icon-lock', 'cloud computing' => 'icon-cloud', 'server' => 'icon-desktop', 'conferencing & voip' => 'icon-bullhorn', 'backup & dr' => 'icon-refresh');
	
		if ($iconDict[$catName] != ""){
		
			return $iconDict[$catName];
		
		}
		else{
		
			return 'icon-table';
		
		}
		
	
	}
	
	add_shortcode('blueprintIntroSections', 'blueprintSections');
	
	function blueprintSections(){
	
		$bpMenuID = get_page_by_title('Blueprints')->ID;
	
		$bpSections = get_pages(array('sort_column' => 'menu_order', 'sort_order' => 'asc', "child_of" => $bpMenuID));
	
		$secCnt = 0;
		
		$sectStr = '<div class="row-fluid">';
		
		foreach($bpSections as $section){
		
			if (($secCnt % 4) == 0 && $secCnt != 0) {
			
				$sectStr .= '</div><div class="row-fluid">';
			
			}
			
			$sectStr .= '<div class="span3">
							<div class="intro_sections">
								<a href="' . get_page_link($section->ID) . '">
									<div class="intro-icon-disc cont-large intro-icon rotate">
										<i class="' . get_post_category_icon(strtolower($section->post_title)) . ' intro-icon-large"></i>
									</div>
									<h3>
										<strong>
										'. $section->post_title .'
										</strong>
									</h3>
									<div class="pad15">
									</div>
								</a>
							</div>
						</div>';
		
			++$secCnt;
		}
		
		if (($secCnt % 4) == 0 && $secCnt != 0) {
			
			$sectStr .= '</div><div class="row-fluid">';
		
		}
		
		//// Adds All Blueprints Section
		$sectStr .= '<div class="span3">
							<div class="intro_sections">
								<a href="' . get_page_link($bpMenuID) . '">
									<div class="intro-icon-disc cont-large intro-icon rotate">
										<i class="' . get_post_category_icon('') . ' intro-icon-large"></i>
									</div>
									<h3>
										<strong>
										All BluePrints
										</strong>
									</h3>
									<div class="pad15">
									</div>
								</a>
							</div>
						</div>';
	
		$secStr .= "</div>";
	
		
		return $sectStr;
	
	}
	

?>