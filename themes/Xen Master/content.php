<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	

	<?php the_content(); ?>
		
	
	<div class="bp-comments">
	
		<?php if ( comments_open() && is_single() ) : ?>
		
		<div class="row-fluid">
		
			<div class="span12">
				<?php if (is_user_logged_in()) :?>
				<?php comments_number('<h4>There are no comments. Be the first to <a href="#" class="jump-comment-form-button">add a commment:</a></h4>', '<h4>There is one comment, <a href="#" class="jump-comment-form-btn">add another comment</a></h4>', '<h4>There are % comments. <a href="#" class="jump-comment-form-btn">Add a comment</a>:</h4>');  ?>
				<?php else : ?>
					<h4>
					<?php comments_number('<h4>There are no comments, ', '<h4>There is one comment, ', '<h4>There are % comments, ');  ?>
					<?php echo sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ); ?>
					</h4>
				<?php endif;?>
			</div>
		
		</div>
		
		<?php if (get_comments_number() != 0) : ///Write Comments ?>
		
		<?php 
			$comments = get_comments('post_id='. get_the_ID());
			$cIdx = 0;
			foreach($comments as $comment) :
				if (($cIdx % 2) == 0):
					$comment_output = '<div id="comment-' . $comment->comment_ID. '" class="media"><div class="pull-left"><i class="icon-comment"></i></div><div class="media-body span8"><p class="media-heading"><strong>' . $comment->comment_author .'</strong><br/>' . date("F j, Y, g:i a", strtotime($comment->comment_date)) . '</p>' . $comment->comment_content . '</div></div>';
				else :
					$comment_output = '<div id="comment-' . $comment->comment_ID. '" class="media alt"><div class="pull-left"><i class="icon-comment"></i></div><div class="media-body span8"><p class="media-heading"><strong>' . $comment->comment_author .'</strong><br/>' . date("F j, Y, g:i a", strtotime($comment->comment_date)) . '</p>' . $comment->comment_content . '</div></div>';
				endif;
				echo($comment_output);
				++$cIdx;
			endforeach;
		?>
		<?php endif; ?>
		
		
		<?php if (is_user_logged_in()) : /// Writes Comment Form ?>
		
		<hr/ id="add-comment-section">
		
		<div class="row-fluid">
		
			<div class="span8">
			
				<?php 
				
					$current_user = wp_get_current_user();
				
					$fields = array(
					
						'comment_field' => '<div class="row-fluid"><div class="span12"><label for="comment">' . _x( 'Comment', 'noun') . '</label></div></div><div class="row-fluid"><div class="span12"><textarea id="comment" class="span12" name="comment" rows="8" aria-required="true"></textarea></div></div>',
						'logged_in_as' => '<div class="row-fluid"><div class="span12">Logged in as <strong>' . $current_user->user_login . '</strong> (<a href="' . wp_logout_url( get_permalink() ) . '">Logout?</a>)</div></div>',
						'title_reply' => '',
						'comment_notes_after' => '',
						'id_submit' => 'comment-submit'
					);
				
				comment_form($fields); 
				
				
				?>
			
			</div>
		
		</div>
		
		<?php endif; ?>
		
		<?php endif; // comments_open() ?>
	
	
	</div>
	
	
</article><!-- #bp-details -->