
</div>
<!--//page-->

<!--footer-->
<div id="footer">
	<div class="container">
		<div class="row">
				
				<!--column 1-->
				<div class="span4">
					<h6>Popular BluePrints</h6>
					<ul id="topRated" class="media-list">
						<?php get_popular_posts(3); ?>
					</ul>
					
				</div>
				<!--column 2-->		
				<div class="span4">
					<h6>Recent Addition</h6>

					<?php echo getRecentBluePrintFooter(); ?>
					
				</div>
				<!--column 3-->
				<div class="span4">
					<!--social-->
					<h6>Connect with Xen Master</h6>
					
				
					<h5></h5>
					<div class="follow_us">
								<a href="#" class="zocial twitter"><i class="icon-twitter"></i></a>
								<a href="#" class="zocial facebook"><i class="icon-facebook"></i></a>
								<a href="#" class="zocial linkedin"><i class="icon-linkedin"></i></a>
								<a href="#" class="zocial googleplus"><i class="icon-google-plus"></i></a>
							</div>
							<div class="copyright">
							Xen Master
							&copy;
							<script type="text/javascript">
								var d = new Date()
								document.write(d.getFullYear())
								</script>
							 - All Rights Reserved<br/>
							Developed by <a href="http://www.pauhanaweb.com">Pau Hana Web</a>
					</div>
				</div>
			</div>
	</div>
</div>
<!--//footer-->
		<!-- up to top -->
		<a href="#">
		<i class="go-top hidden-phone hidden-tablet  icon-double-angle-up"></i></a>
		<!--//end-->
<script>
	var ajURL = "<?php bloginfo('wpurl'); ?>/wp-admin/admin-ajax.php";
	var ajNonce = "<?php echo js_escape( wp_create_nonce('ajax_referrer_nonce')); ?>";
</script>
<script src="<?php bloginfo('template_url') ?>/js/jquery.touchSwipe.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/jquery.mousewheel.min.js"></script>				
<script src="<?php bloginfo('template_url') ?>/js/jquery.nivo.slider.pack.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jquery.easing.1.3.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/superfish.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jquery.rateit.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/scripts.js"></script>
</body>
</html>