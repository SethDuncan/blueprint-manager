<div class="wrap" >
	<h1> Blueprint Manager - New Blueprint</h1>
		<div id-"mainblock" >
			<div class="dbx-content">
				<form action="<?php echo $action_url ?>" method="post" enctype="multipart/form-data">
					<?php wp_nonce_field('phes-nonce'); ?>
						<div style="display:<?php echo $tableDisplay; ?>;">
							
							<h2>Required Blueprint Details</h2>
						
							<table>
								
								<tr>
									<td>Blueprint Image</td>
									<td><input type="file" name="file[]" id="file"></td>
								</tr>
								<tr>
									<td>Description</td>
									<td><input type="text" name="txtDescription" class="validate-req" size="75" value=""></td>
								</tr>
								<tr>
									<td>Notes</td>
									<td><textarea id="txtNotes" rows="4" cols="75" class="validate-req" name="txtNotes"></textarea></td>
								</tr>
								
								<tr>
									<td>Category</td>
									<td><select name="selCategory">
										<?php foreach($categories as $category){?>
											<option value="<?php echo esc_attr($category->ID);?>">
												<?php echo esc_html($category->Description); ?>
											</option>
										<?php } ?>
									</td>
								</tr>
								
							</table>
							<br/>
							<br/>
							<h2>Additional Information</h2>
							
							<table>
								
								<tr>
									<td>Architect</td>
									<td><input type="text" name="txtArchitect" size="60" value=""></td>
								</tr>
								<tr>
									<td>Blueprint Verified By</td>
									<td><input type="text" name="txtBlueprintVerifiedBy" size="60" value=""></td>
								</tr>
								<tr>
									<td>Blueprint Certified By</td>
									<td><input type="text" name="txtBlueprintCertifiedBy" size="60" value=""></td>
								</tr>
								<tr>
									<td>Configuration Verified By</td>
									<td><input type="text" name="txtConfigVerifiedBy" size="60" value=""></td>
								</tr>
								<tr>
									<td>Configuration Certified By</td>
									<td><input type="text" name="txtConfigCertifiedBy" size="60" value=""></td>
								</tr>						
								<tr>
									<td>Status</td>
									<td><select name="selStatus">
										<?php foreach($statuses as $status){?>
											<option value="<?php echo esc_attr($status->ID);?>">
												<?php echo esc_html($status->Description); ?>
											</option>
										<?php } ?>
									</td>
								</tr>
								<tr>
									<td>Industry</td>
									<td><select name="selIndustry">
										<?php foreach($industries as $industry){?>
											<option value="<?php echo esc_attr($industry->ID);?>">
												<?php echo esc_html($industry->Description); ?>
											</option>
										<?php } ?>
									</td>
								</tr>
								
								<!--<tr>
									<td>Privacy</td>
									<td><select name="selPrivacy">
										<?php //foreach($privacies as $privacy){?>
											<option value="<?php //echo esc_attr($privacy->ID);?>">
												<?php //echo esc_html($privacy->Description); ?>
											</option>
										<?php //} ?>
									</td>
								</tr>-->
								<tr>
									<td>Revision</td>
									<td><input type="text" name="txtRevision" size="50" value=""></td>
								</tr>
								<tr>
									<td>URL</td>
									<td><input type="text" name="txtURL"  size="75" value=""></td>
								</tr>
								<tr>
									<td>Date Submitted</td>
									<td>
										<input id="txtDateBox" type="text" name="txtFriendlyDate" size="75" class="date-field-other" value="">
										<input id="txtDateBoxOther" type="hidden" name="txtDate" value="">
									</td>
								</tr>
								<tr>
									<td>Estimated Hardware Cost</td>
									<td><input type="text" name="txtEstHardware" size="50" value=""></td>
								</tr>
								<tr>
									<td>Estimated Software Cost</td>
									<td><input type="text" name="txtEstSoftware" size="50" value=""></td>
								</tr>
								<tr>
									<td>Estimated Configuration Time</td>
									<td><input type="text" name="txtEstConfigTime" size="50" value=""></td>
								</tr>
								<tr>
									<td>Estimated Configuration Cost</td>
									<td><input type="text" name="txtEstConfigCost" size="50" value=""></td>
								</tr>
								
								<tr>
									<td>Attachment 1 Name</td>
									<td><input type="text" name="txtAttachment1Name" size="60" value=""></td>
								</tr>
								<tr>
									<td>Attachment 1</td>
									<td><input type="file" name="file[]" id="file"></td>
								</tr>
								<tr>
									<td>Attachment 2 Name</td>
									<td><input type="text" name="txtAttachment2Name" size="60" value=""></td>
								</tr>
								<tr>
									<td>Attachment 2</td>
									<td><input type="file" name="file[]" id="file"></td>
								</tr>
								<tr>
									<td>Attachment 3 Name</td>
									<td><input type="text" name="txtAttachment3Name" size="60" value=""></td>
								</tr>
								<tr>
									<td>Attachment 3</td>
									<td><input type="file" name="file[]" id="file"></td>
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" name="btnNew" value="Add New Blueprint" /></td>
								</tr>
							</table>
							<br><?php echo $returnLink; ?><br>
						</div>
				</form>
			</div>
		</div>
</div>
<script>
	(jQuery)(document).ready(function (){

		(jQuery)("#txtDateBox").datepicker('setDate', new Date());

	});
</script>
						