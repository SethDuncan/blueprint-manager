<?php
/*
Plugin Name: Blueprint Manager
Version: 1.0
Description: Display Database Blueprints for Xen Master Site
Author: Barrett Darnell & Seth Duncan
Author URI: http://www.pauhanaweb.com/
Plugin URI: http://www.pauhanaweb.com/
*/

/* Version check */
global $wp_version;



$exit_msg = 'Blueprint Manager requires WordPress 3.6.1 or newer. <a href="http://codex.wordpress.org/Upgrading_Wordpress">Please update!</a>';
if (version_compare($wp_version, "3.6.1" ,"<")){
	exit($exit_msg);
}

if (!class_exists('BlueprintManager')):
	class BlueprintManager{
		//this varible will hold the url to the plugin
		var $plugin_url;
		var $upload_dir;
		var $file_move_dir;
		
		//initialize the plugin
		function BlueprintManager(){
			$this->plugin_url = trailingslashit(get_bloginfo('wpurl')) . PLUGINDIR . '/' . dirname(plugin_basename(__FILE__));
			$this->file_move_dir = '../' . PLUGINDIR . '/' . dirname(plugin_basename(__FILE__)). "/uploads/";
			$this->upload_dir =  $this->plugin_url . "/uploads/";
			

			//hook the shortcode for menu display
			add_shortcode('display_blueprint_menu', array(&$this, 'displayBlueprintMenu'));
			
			//hook the shortcode for blueprint display
			add_shortcode('display_blueprint', array(&$this, 'displayBlueprint'));
			
			//hook the admin menus
			add_action('admin_menu', array(&$this, 'create_menu'));
			
			//hook the scripts included to include Scripts
			add_action('admin_init', array($this, 'phbm_admin_init'));
			
			//hook the sitewide enqueue to add phbm-validation
			add_action( 'wp_enqueue_scripts', array($this, 'phbm_enqueue_scripts') );

			//hook into custom user data (metadata)
			add_filter('user_contactmethods', array(&$this, 'customUserData'));
			//hook to remove color picker
			remove_action("admin_color_scheme_picker", "admin_color_scheme_picker");
			//buffer actions to remove bio, nickname, display name, website

		}
		
		function customUserData( $user ) {
			$user_contactmethods['phone'] = 'Phone Number';
			$user_contactmethods['companyname'] = 'Company Name';
			$user_contactmethods['address'] = 'Address';
			$user_contactmethods['facebook'] = 'Facebook Page';
			$user_contactmethods['linkedin'] = 'LinkedIn Page';
			$user_contactmethods['twitter'] = 'Twitter Page';
			return $user_contactmethods;
		}

		function phbm_enqueue_scripts() {
			// put javascript here
			//seth these are you last plugins, we will probably use a version of those
			//wp_enqueue_script('phbm-valid', $this->plugin_url. '/js/phbm-validate.js', array( 'jquery' ));
			//wp_enqueue_script('jq-spin', $this->plugin_url. '/js/spin.min.js', array('jquery'));
		}
		
		function phbm_admin_init(){
			
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-ui-core');
			wp_enqueue_script('jquery-ui-datepicker', array('jquery', 'jquery-ui-core') );
			/*wp_enqueue_script('jquery-ui-slider', array('jquery', 'jquery-ui-core') );
			wp_enqueue_script('jquery-ui-dialog', array('jquery', 'jquery-ui-core') );
			wp_enqueue_script('jq-datatables', $this->plugin_url. '/js/jquery.dataTables.min.js', array('jquery') );
			wp_enqueue_script('jq-timepicker', $this->plugin_url. '/js/jquery-ui-timepicker-addon.js', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker', 'jquery-ui-slider'));
			*/
			wp_enqueue_script('phbm-valid', $this->plugin_url. '/js/phbm-validate.js', array('jquery'));
			wp_enqueue_script('admin-ui-scripts', $this->plugin_url. '/js/admin-ui-scripts.js', array('jquery', 'jquery-ui-core'));
			
			/*
			wp_localize_script( 'admin-ui-scripts', 'ajax_properties', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
			*/
			
			wp_enqueue_style('jquery.ui.theme', $this->plugin_url . '/styles/jquery-ui-1.10.3.custom.min.css');
			wp_enqueue_style('admin-styles', $this->plugin_url .'/styles/admin-styles.css');
			/*wp_enqueue_style('datatables.styles', $this->plugin_url . '/styles/jquery.dataTables_themeroller.css');
			wp_enqueue_style('print.styles', $this->plugin_url . '/styles/print.css');

			*/
		}
		
		function create_menu(){
			//add administrative menu page
			add_menu_page('Settings', 'Blueprint Manager', 'administrate_phbm', 'phbm_manage_settings', array(&$this, 'manageSettings'), $this->plugin_url . '/styles/images/icon-blueprint.png');
			add_submenu_page('phbm_manage_settings', 'New Blueprint', 'New Blueprint', 'use_phbm', 'phbm_new_blueprint', array(&$this, 'newBlueprint'));
			add_submenu_page('phbm_manage_settings', 'Manage Blueprints', 'Manage Blueprints', 'administrate_phbm', 'phbm_manage_blueprints', array(&$this, 'manageBlueprints'));
			add_submenu_page(Null, 'Edit Bluepint', 'Edit Blueprint', 'administrate_phbm', 'phbm_edit_blueprint', array(&$this, 'editBlueprint'));
		}
		
		function activate(){
			//create the view blueprint page
			global $wpdb;
			$query = $wpdb->prepare(
				"SELECT ID FROM " . $wpdb->posts . "
				WHERE post_title = 'View Blueprint'
				AND post_type = 'page'"
			, '');
			$wpdb->query( $query );

			if ( !($wpdb->num_rows)) {
				$post = array(
					'comment_status' => 'closed',
					'post_author'    => 1,
					'post_content'   => "[display_blueprint]",
					'post_name'      => "View_Blueprint",
					'post_status'    => 'publish',
					'post_title'     => 'View Blueprint',
					'post_type'      => 'page',
					'menu_order'     => 2
				 ); 
				 
				 $post_id = wp_insert_post( $post, $wp_error );
				 add_option( 'view_blueprint_id', $post_id, '', 'yes' );
			}
			
			$query = $wpdb->prepare(
				"SELECT ID FROM " . $wpdb->terms . "
				WHERE name = 'Blueprint'"
			, '');
			$wpdb->query( $query );
			
			if ( !($wpdb->num_rows)) {
				$id = wp_create_category('Blueprint', 0);
				
				add_option( 'view_blueprint_category', $id, '', 'yes' );
			}
			
			
			
			//add administrate_phbm capability to admin roles
			$role =& get_role('administrator');
			if (!empty($role)){
				$role->add_cap('administrate_phbm');
				$role->add_cap('use_phbm');
			}
			
			//add use_phbm capability to admin roles
			$role =& get_role('subscriber');
			if (!empty($role)){
				$role->add_cap('use_phbm');
			}
			
			update_option('permalink_structure', "/%postname%/");
			
			//make upload directory
			@mkdir($this->upload_dir, 0777);
		}
			
		function deactivate(){
			//remove shortcode and menus
			remove_shortcode('display_blueprint_menu');
			remove_shortcode('display_blueprint');
			
			//remove viewblurprint page
			wp_delete_post(get_option('view_blueprint_id'), true);
			delete_option('view_blueprint_id' );
			delete_option('view_blueprint_category' );
			
			//remove custom admin menu
			remove_submenu_page('phbm_manage_settings','phbm_new_blueprint');
			remove_submenu_page('phbm_manage_settings', 'phbm_manage_blueprints');
			remove_menu_page('phbm_manage_settings');
			
			//remove capability from admin
			$role =& get_role('administrator');
			if (!empty($role)){
				$role->remove_cap(administrate_phbm);
				$role->add_cap('use_phbm');
			}
			
			//remove use_phbm capability from admin
			$role =& get_role('subscriber');
			if (!empty($role)){
				$role->remove_cap(use_phbm);
			}
		}
		
		function uninstall(){
			//action for uninstall code
		}
				
		function displayBlueprintMenu( $params ){
			//get short code parameter of category into $category
			extract( shortcode_atts( array('category' => '1'), $params ) );

			$bpPage = get_page_by_title('Blueprints');
			$bpPageURL = get_page_link($bpPage->ID);
			$curPage = get_query_var('pagename');
			
			
			//get all blueprints of that category and display table
			$sqlResults = $this->selectBlueprintsbyCategoryApproved($category);
			
			if (!empty($sqlResults)){
				$displayTable = '
				<div class="breadcrumbs"><a href="'. $bpPageURL .'">Blueprints</a> <i class="icon-double-angle-right grey"></i>'. $curPage .'</div>
				<br/>
				<div class="row-fluid">
					<div class="span12">
						<table class="wp-list-table widefat bpListTable fixed data-table-enabled dt-ed" cellspacing="0">
							<thead>
								<tr>
									<th scope="col">Description</th>
									<th scope="col">Architect</th>
									<th scope="col">Verified By</th>
									<th scope="col">Status</th>
									<th scope="col">Added</th>
									<th scope="col">Rating</th>
								</tr>
							</thead>
							<tbody>';
				
				foreach($sqlResults as $result){
					$displayTable .= '<tr >
								<td><a href="' . wp_nonce_url(get_permalink($result->Post_FK), "phbm-nonce") . '">' . $result->Description . '</a></td>
								<td>' . $result->Architect . '</td>
								<td>' . $result->BlueprintVerifiedBy . '</td>
								<td>' . $result->StatusDescription . '</td>
								<td>' . date("F j, Y", strtotime($result->DateSubmitted)) . '</td>';
				
				
					$ratingResult = $this->selectRatingforBlueprint($result->ID);
					if (!empty($ratingResult)){
						$ratingAv = floor(($ratingResult[0]->AvgRating) * 100)/100;
					}
					else {
						$ratingAv = 0;
					}
				
					
					
					if ($ratingAv == 0){
						$displayTable .= '<td>N/A</td></tr>';
					}
					else{
						$displayTable .= '<td>'. $ratingAv .'/5</td></tr>';
					}
				
				}
				$displayTable .= '
						</tbody>
						</table>
						</div>
						</div>
						<div class="row-fluid">
						<div class="span12"><br/>';
					if(is_user_logged_in()) {
						//// Redirect to Backend page
						$addURL = admin_url('admin.php?page=phbm_new_blueprint');
						$displayTable .= '<a class="btn btn-primary btn-large" href="'. $addURL . '">Add your own IT BluePrint</a>';
					}
					else{
						/// Redirect to Login/Register Page
						$loginURL = wp_login_url(admin_url('admin.php?page=phbm_new_blueprint'));
						$displayTable .= '<a class="btn btn-primary btn-large" href="'. $loginURL . '">Log in to add your own IT Blueprint</a>';
					}
					
					
					$displayTable .= '
					</div>
					</div>';
			}
			else{
			
				$displayTable .= '
				<div class="breadcrumbs"><a href="'. $bpPageURL .'">Blueprints</a> <i class="icon-double-angle-right grey"></i>'. $curPage .'</div>
				<br/>
				<div class="row-fluid">
					<div class="span12 text-center">
						<h1> No Blueprints in this category</h1>';
				
				if(is_user_logged_in()) {
					//// Redirect to Backend page
					$addURL = admin_url('admin.php?page=phbm_new_blueprint');
					$displayTable .= '<a class="btn btn-primary btn-large" href="'. $addURL . '">Add your own IT BluePrint</a>';
				}
				else{
					/// Redirect to Login/Register Page
					$loginURL = wp_login_url(admin_url('admin.php?page=phbm_new_blueprint'));
					$displayTable .= '<a class="btn btn-primary btn-large" href="'. $loginURL . '">Log in to add your own IT Blueprint</a>';
				}
				
				
				$displayTable .= '
					</div>
				</div>';
			
			}
			
			return $displayTable;
			
			//logged in user code
			/*
			if (!is_user_logged_in()) {
				
				echo '<div class="row-fluid"><div class="span12"><div class="pull-right">You are not currently logged in(<a href="' . wp_login_url( site_url( $_SERVER['REQUEST_URI'] ), false ) . '" title="Login">Log in</a>)</div></div></div>';
			} else {
				$current_user = wp_get_current_user();
				echo '<div class="row-fluid"><div class="span12"><div class="pull-right">Signed in as 
															<div class="btn-group">
																<button class="btn">' . $current_user->user_firstname . ' ' . $current_user->user_lastname . '</button> 
																<button class="btn dropdown-toggle" data-toggle="dropdown">
	    															<span class="caret"></span>
	  															</button>
	  															<ul class="dropdown-menu">
	  																<li>
	  																	<a href="' . wp_logout_url( get_permalink() ) . '" title="Logout" >Logout</a>
																	</li>
																</ul>
															</div>
												</div></div></div>';
			}
			*/
		}
		
		function displayBlueprint($params){
			if (!isset($params["id"])){
				//no blueprint requested, display list all blueprints
				$sqlResults = $this->selectBlueprintsApproved();
				
				if(!empty($sqlResults)){
				
					$displayTable = '
						<div class="breadcrumbs">Blueprints <i class="icon-double-angle-right grey"></i>All Blueprints</div>
						<br/>
						<div class="row-fluid">
							<div class="span12">
								<table class="wp-list-table bpListTable widefat fixed data-table-enabled dt-ed" cellspacing="0">
										<thead>
											<tr>
												<th scope="col">Category</th>
												<th scope="col">Description</th>
												<th scope="col">Architect</th>
												<th scope="col">Verified By</th>
												<th scope="col">Status</th>
												<th scope="col">Added</th>
												<th scope="col">Added</th>
											</tr>
										</thead>
										<tbody>';
					
					foreach($sqlResults as $result){
						$displayTable .= '<tr >
									<td>' . $result->CategoryDescription . '</td>
									<td><a href="' . wp_nonce_url(get_permalink($result->Post_FK), "phbm-nonce") . '">' . $result->Description . '</a></td>
									<td>' . $result->Architect . '</td>
									<td>' . $result->BlueprintVerifiedBy . '</td>
									<td>' . $result->StatusDescription . '</td>
									<td>' . date("F j, Y", strtotime($result->DateSubmitted)) . '</td>';
									
									$ratingResult = $this->selectRatingforBlueprint($result->ID);
									
									if (!empty($ratingResult)){
										$ratingAv = floor(($ratingResult[0]->AvgRating) * 100)/100;
									}else {
										$ratingAv = 0;
									}
								
									if ($ratingAv == 0){
										$displayTable .= '<td>N/A</td></tr>';
									}else{
										$displayTable .= '<td>'. $ratingAv .'/5</td></tr>';
									}
													
					}
					$displayTable .= '
						</tbody>
						</table>
						</div>
						</div>
						<div class="row-fluid">
						<div class="span12"><br/>';
					if(is_user_logged_in()) {
						//// Redirect to Backend page
						$addURL = admin_url('admin.php?page=phbm_new_blueprint');
						$displayTable .= '<a class="btn btn-primary btn-large" href="'. $addURL . '">Add your own IT BluePrint</a>';
					}
					else{
						/// Redirect to Login/Register Page
						$loginURL = wp_login_url(admin_url('admin.php?page=phbm_new_blueprint'));
						$displayTable .= '<a class="btn btn-primary btn-large" href="'. $loginURL . '">Add your own IT BluePrint</a>';
					}
					
					
					$displayTable .= '
					</div>
					</div>';
				}
				else{
				
					$displayTable .= '
					<div class="breadcrumbs"><a href="'. $bpPageURL .'">Blueprints</a> <i class="icon-double-angle-right grey"></i>'. $curPage .'</div>
					<br/>
					<div class="row-fluid">
						<div class="span12 text-center">
							<h1> No Blueprints in this category</h1>';
					
					
					
					$displayTable .= '
						</div>
					</div>';
				
				}
				return $displayTable;
			}else{
				//get requested blueprint and display
				$sqlResults = $this->selectBlueprintbyID($params["id"]);
				
				$ratingResult = $this->selectRatingforBlueprint($params["id"]);
				if (!empty($ratingResult)){
					$ratingAv = floor(($ratingResult[0]->AvgRating) * 100)/100;
				}
				else {
					$ratingAv = 3;
				}
				
				
				$display = '<div class="row-fluid">
								<div class="span10 offset1 text-center">
									<img src="' . $this->upload_dir . $sqlResults[0]->Blueprint . '">
								</div>
							</div>
							<div class="row-fluid">
								<div class="span4 offset4 text-center">
									Rating<div class="rateit" data-rateit-value="'. $ratingAv .'" data-id="'. $params["id"] .'"></div>
								</div>
							</div>
							<div class="row-fluid">
								<div class="span12">
									
								</div>
							</div>
							<div class="row-fluid">
								<div class="span6 bpInfoSection">
									<div class="bpInfoHeading">
										<i class="icon-info"></i>Blueprint Information
									</div>
									<table>					
										<tr>
											<td style="width: 45%;"><strong>Description</strong></td>
											<td>' . $sqlResults[0]->Description . '</td>
										</tr>
										<tr>
											<td><strong>Architect</strong></td>
											<td>' . $sqlResults[0]->Architect . '</td>
										</tr>
										<tr>
											<td><strong>Blueprint Verified By</strong></td>
											<td>' . $sqlResults[0]->BlueprintVerifiedBy . '</td>
										</tr>
										<tr>
											<td><strong>Blueprint Certified By</strong></td>
											<td>' . $sqlResults[0]->BlueprintCertifiedBy . '</td>
										</tr>
										<tr>
											<td><strong>Configuration Verified By</strong></td>
											<td>' . $sqlResults[0]->ConfigVerifiedBy . '</td>
										</tr>
										<tr>
											<td><strong>Configuration Certified By</strong></td>
											<td>' . $sqlResults[0]->ConfigCertifiedBy . '</td>
										</tr>
										<tr>
											<td><strong>Status</strong></td>
											<td>' . $sqlResults[0]->StatusDescription . '</td>
										</tr>
										<tr>
											<td><strong>Industry</strong></td>
											<td>' . $sqlResults[0]->IndustryDescription . '</td>
										</tr>
										<tr>
											<td><strong>Category</strong></td>
											<td>' . $sqlResults[0]->CategoryDescription . '</td>
										</tr>
										<tr>
											<td><strong>Revision</strong></td>
											<td>' . $sqlResults[0]->Revision . '</td>
										</tr>
										<tr>
											<td><strong>Date Submitted</strong></td>
											<td>' . date("F j, Y", strtotime($sqlResults[0]->DateSubmitted)) . '</td>
										</tr>
										<tr>
											<td><strong>URL</strong></td>
											<td><a href="' . $sqlResults[0]->URL . '" target="_blank">' . $sqlResults[0]->URL . '</a></td>
										</tr>
									</table>
								</div>
								<div class="span6">
									<div class="row-fluid">
										<div class="span12 bpInfoSection">
											<div class="bpInfoHeading">
												<i class="icon-time"></i>Time/Cost
											</div>
											<table>
												<tr>
													<td style="width: 40%;"><strong>Estimated Hardware Cost</strong></td>
													<td>' . $sqlResults[0]->EstHardware . '</td>
												</tr>
												<tr>
													<td><strong>Estimated Software Cost</strong></td>
													<td>' . $sqlResults[0]->EstSoftware . '</td>
												</tr>
												<tr>
													<td><strong>Estimated Configuration Time</strong></td>
													<td>' . $sqlResults[0]->EstConfigTime . '</td>
												</tr>
												<tr>
													<td><strong>Estimated Configuration Cost</strong></td>
													<td>' . $sqlResults[0]->EstConfigCost . '</td>
												</tr>
											</table>
										</div>
									</div>
									<div class="row-fluid">
										<div class="span12 bpInfoSection">
										
											<div class="bpInfoHeading">
													<i class="icon-paper-clip"></i>
													Attachments
											</div>
											<table>
												<tr>
													<td style="width: 15%;"><strong>Attachments</strong></td>
													<td>
														<ul>';
							
						if (strlen($sqlResults[0]->Attachment1)){
							$display .= '<li><a href="' . $this->upload_dir . $sqlResults[0]->Attachment1 . '" target="_blank">1. ' . $sqlResults[0]->Attachment1Name . '</li>';
						}
						if (strlen($sqlResults[0]->Attachment2)){
							$display .= '<li><a href="' . $this->upload_dir . $sqlResults[0]->Attachment2 . '" target="_blank">2. ' . $sqlResults[0]->Attachment2Name . '</li>';
						}
						if (strlen($sqlResults[0]->Attachment3)){
							$display .= '<li><a href="' . $this->upload_dir . $sqlResults[0]->Attachment3 . '" target="_blank">3. ' . $sqlResults[0]->Attachment3Name . '</li>';
						}
						$display .= '</ul>
													</td>
												</tr>
						
						
												
											</table>
										
										</div>
									</div>
								</div>
							</div>
							<div class="row-fluid">
								<div class="span12 bpInfoSection">
									<div class="bpInfoHeading">
										<i class="icon-comment"></i>
										Additional Info
									</div>
									<table>
										<tr>
											<td style="width: 15%;"><strong>Notes</strong></td>
											<td>' . $sqlResults[0]->Notes . '</td>
										</tr>
									</table>
								</div>
							</div>';
				return $display . $content;
			}
		}
		
		function manageSettings(){
			global $wpdb;
			$categories = $this->selectCategories();
			include('ph-blueprint-manager-settings.php');
		}
		
		function manageBlueprints(){
			//responding to page post, looking for $_POST and $_GET
			if (isset($_GET['action'])){
				if ($_GET['action']=="delete" && isset($_GET['blueprint'])){
					check_admin_referer('phbm-nonce');
					$result = $this->selectBlueprintbyID($_GET['blueprint']);
					wp_delete_post( $result[0]->Post_FK, true );
					$this->deleteRow("blueprints", $_GET['blueprint']);
					
					echo '<div class="updated fade"><p>Blueprint deleted.</p></div>';
				}
			}

			//create table to display
			$sqlResults = $this->selectBlueprints();
			$displayTable = '<table class="wp-list-table widefat fixed data-table-enabled dt-ed bp-manage" cellspacing="0">
						<thead>
							<tr>
								<th scope="col">Description</th>
								<th scope="col">Architect</th>
								<th scope="col">Date</th>
								<th scope="col">Status</th>
								<th scope="col">Industry</th>
								<th scope="col">Category</th>
								<th scope="col">Reviewed?</th>
								<th scope="col">Edit</th>
								<th scope="col">Delete</th>
							</tr>
						</thead>
						<tbody>';
			
			foreach($sqlResults as $result){
				$displayTable .= '<tr >
							<td>' . $result->Description . '</td>
							<td>' . $result->Architect . '</td>
							<td>' . date("m/j/Y", strtotime($result->DateSubmitted)) . '</td>
							<td>' . $result->StatusDescription . '</td>
							<td>' . $result->IndustryDescription . '</td>
							<td>' . $result->CategoryDescription . '</td>';
				
				$reviewClass = 'yuppers';
				switch ($result->Review_FK) {
				
					case 1:
						$reviewClass = 'not-reviewed';
					break;
					case 2:
						$reviewClass = 'rejected';
					break;
					case 3:
						$reviewClass = 'approved';
					break;
					default: 
						$reviewClass = 'not-reviewed';
					break;
				
				}
				if ($result->Review_FK == 1){
					$displayTable .= '<td class="'. $reviewClass .'"><a class="button-primary" href="'. wp_nonce_url("admin.php?page=phbm_edit_blueprint&blueprint=" . $result->ID, "phbm-nonce") .'">' . $result->ReviewDescription . '</a></td>
								<td><a class="button-primary" href="' . wp_nonce_url("admin.php?page=phbm_edit_blueprint&blueprint=" . $result->ID, "phbm-nonce") . '">Edit</a></td>
								<td><a class="button-primary" href="' . wp_nonce_url("admin.php?page=phbm_manage_blueprints&action=delete&blueprint=" . $result->ID, "phbm-nonce") . '">Delete</a></td>';
				}
				else{
					$displayTable .= '<td class="'. $reviewClass .'">' . $result->ReviewDescription . '</td>
								<td><a class="button-primary" href="' . wp_nonce_url("admin.php?page=phbm_edit_blueprint&blueprint=" . $result->ID, "phbm-nonce") . '">Edit</a></td>
								<td><a class="button-primary" href="' . wp_nonce_url("admin.php?page=phbm_manage_blueprints&action=delete&blueprint=" . $result->ID, "phbm-nonce") . '">Delete</a></td>';
				}
			}
			$displayTable .= '
				</tbody>
					<tfoot>
						<tr>
							<th scope="col">Description</th>
							<th scope="col">Architect</th>
							<th scope="col">Date</th>
							<th scope="col">Status</th>
							<th scope="col">Industry</th>
							<th scope="col">Category</th>
							<th scope="col">Reviewed?</th>
							<th scope="col">Edit</th>
							<th scope="col">Delete</th>
						</tr>
					</tfoot>
				</table>';
			include('ph-blueprint-manager-manage-blueprints.php');
		}
		
		function newBlueprint(){
			//responding to page post, looking for $_POST and $_GET
			$tableDisplay = "";
			
			
			if (isset($_POST['btnNew'])){
				if ($_FILES["file"]["size"][0] > 0 ){
					//first try the file upload
					try{
						//$filenameThumbnail = $this->fileUpload($_FILES["file"]["name"][0], $_FILES["file"]["size"][0], $_FILES["file"]["error"][0], $_FILES["file"]["tmp_name"][0]);
						//$filenameFullImage = $this->fileUpload($_FILES["file"]["name"][1], $_FILES["file"]["size"][1], $_FILES["file"]["error"][1], $_FILES["file"]["tmp_name"][1]);
						$filenameFullImage = $this->fileUpload($_FILES["file"]["name"][0], $_FILES["file"]["size"][0], $_FILES["file"]["error"][0], $_FILES["file"]["tmp_name"][0]);
						$filenameAttachment1 = '';
						$filenameAttachment2 = '';
						$filenameAttachment3 = '';
						
						if ($_FILES["file"]["size"][2] > 0){
							//attachment 1 selected
							$filenameAttachment1 = $this->fileUpload($_FILES["file"]["name"][1], $_FILES["file"]["size"][1], $_FILES["file"]["error"][1], $_FILES["file"]["tmp_name"][1]);	
						}
						if ($_FILES["file"]["size"][3] > 0){
							//attachment 1 selected
							$filenameAttachment2 = $this->fileUpload($_FILES["file"]["name"][2], $_FILES["file"]["size"][2], $_FILES["file"]["error"][2], $_FILES["file"]["tmp_name"][2]);	
						}
						if ($_FILES["file"]["size"][4] > 0){
							//attachment 1 selected
							$filenameAttachment3 = $this->fileUpload($_FILES["file"]["name"][3], $_FILES["file"]["size"][3], $_FILES["file"]["error"][3], $_FILES["file"]["tmp_name"][3]);	
						}
						
						
							//now do the massive insert, giggity, privacy is forced to 1
						$result = $this->insertBlueprint($filenameFullImage,
							$_POST['txtArchitect'],
							$_POST['txtURL'],
							$_POST['txtDate'],
							$_POST['txtDescription'],
							$_POST['txtNotes'],
							$_POST['txtRevision'],
							$_POST['selStatus'],
							$_POST['selIndustry'],
							1,
							$_POST['selCategory'],
							1,
							$_POST['txtEstHardware'],
							$_POST['txtEstSoftware'],
							$_POST['txtEstConfigCost'],
							$_POST['txtEstConfigTime'],
							$_POST['txtBlueprintVerifiedBy'],
							$_POST['txtBlueprintCertifiedBy'],
							$_POST['txtConfigVerifiedBy'],
							$_POST['txtConfigCertifiedBy'],
							$filenameAttachment1,
							$filenameAttachment2,
							$filenameAttachment3,
							$_POST['txtAttachment1Name'],
							$_POST['txtAttachment2Name'],
							$_POST['txtAttachment3Name'],
							get_current_user_id());
						
						if (1==1){//$result){
							echo '<div class="updated fade"><p>New blueprint added. <br><a href="' . wp_nonce_url("admin.php?page=phbm_new_blueprint", "phbm-nonce") . '">Click here to add another</a><br>
							<a href="' . wp_nonce_url("admin.php?page=phbm_manage_blueprints", "phbm-nonce") . '">Return to Manage Blueprints</a><br>
							</p></div>';
							$tableDisplay = "none";
							$post = array(
								'comment_status' => 'open',
								'post_author'    => get_current_user_id(),
								'post_category'  => array(get_option('view_blueprint_category')),
								'post_content'   => "[display_blueprint id=" . $result . "]",
								'post_name'      => "Blueprint #" . $result,
								'post_status'    => 'publish',
								'post_title'     => 'Blueprint #' . $result,
								'post_type'      => 'post',
							 ); 
							 
							 $post_id = wp_insert_post( $post, $wp_error );
							 $this->updateBlueprintPost($post_id, $result);
						}else{
							echo '<div class="updated fade"><p>Error: Not added, please check values and try again.</p></div>';
						}
					} catch (Exception $ex) {
						echo '<div class="updated fade"><p>Error with file upload. ' . $ex->getMessage() . '</p></div>';
					}
				}else{
					echo '<div class="updated fade"><p>Error: No blueprint image and thumbnail selected</p></div>';
				}
			}

			$statuses = $this->selectStatuses();
			$industries = $this->selectIndustries();
			$categories = $this->selectCategories();
			
			//$privacies = $this->selectPrivacy();
			$returnLink = '<a href="' . wp_nonce_url("admin.php?page=phbm_manage_blueprints", "phbm-nonce") . '">Return to Manage Blueprints (discard changes)</a>';

			include('ph-blueprint-manager-new-blueprint.php');
		}
		
		function editBlueprint(){
			//responding to page post, looking for $_POST and $_GET
			$tableDisplay = "";
			if (isset($_POST['btnSave'])){
				//first try the file upload
				try{
					//save old data
					$sqlResults = $this->selectBlueprintbyID($_GET['blueprint']);
					//$filenameThumbnail = $sqlResults[0]->BlueprintThumbnail;
					$filenameFullImage = $sqlResults[0]->Blueprint;
					$filenameAttachment1 = $sqlResults[0]->Attachment1;
					$filenameAttachment2 = $sqlResults[0]->Attachment2;					
					$filenameAttachment3 = $sqlResults[0]->Attachment3;
					$attachment1Name = $sqlResults[0]->Attachment1Name;
					$attachment2Name = $sqlResults[0]->Attachment2Name;
					$attachment3Name = $sqlResults[0]->Attachment3Name;
						
					/*if ($_FILES["file"]["size"][0] > 0){
						$filenameThumbnail = $this->fileUpload($_FILES["file"]["name"][0], $_FILES["file"]["size"][0], $_FILES["file"]["error"][0], $_FILES["file"]["tmp_name"][0]);
					}*/
					if ($_FILES["file"]["size"][0] > 0){
						$filenameFullImage = $this->fileUpload($_FILES["file"]["name"][0], $_FILES["file"]["size"][0], $_FILES["file"]["error"][0], $_FILES["file"]["tmp_name"][0]);
					}
					if ($_FILES["file"]["size"][1] > 0){
						//attachment 1 selected
						$filenameAttachment1 = $this->fileUpload($_FILES["file"]["name"][1], $_FILES["file"]["size"][1], $_FILES["file"]["error"][1], $_FILES["file"]["tmp_name"][1]);	
						$attachment1Name = $_POST['txtAttachment1Name'];
					}
					if ($_FILES["file"]["size"][2] > 0){
						//attachment 1 selected
						$filenameAttachment2 = $this->fileUpload($_FILES["file"]["name"][2], $_FILES["file"]["size"][2], $_FILES["file"]["error"][2], $_FILES["file"]["tmp_name"][2]);	
						$attachment2Name = $_POST['txtAttachment2Name'];
					}
					if ($_FILES["file"]["size"][3] > 0){
						//attachment 1 selected
						$filenameAttachment3 = $this->fileUpload($_FILES["file"]["name"][3], $_FILES["file"]["size"][3], $_FILES["file"]["error"][3], $_FILES["file"]["tmp_name"][3]);	
						$attachment3Name = $_POST['txtAttachment3Name'];
					}
					
					$result = $this->updateBlueprint(
						$filenameFullImage, 
						$_POST['txtArchitect'],
						$_POST['txtURL'],
						$_POST['txtDate'],
						$_POST['txtDescription'],
						$_POST['txtNotes'],
						$_POST['txtRevision'],
						$_POST['selStatus'],
						$_POST['selIndustry'],
						1,
						$_POST['selCategory'],
						$_POST['selReview'],
						$_POST['txtEstHardware'],
						$_POST['txtEstSoftware'],
						$_POST['txtEstConfigCost'],
						$_POST['txtEstConfigTime'],
						$_POST['txtBlueprintVerifiedBy'],
						$_POST['txtBlueprintCertifiedBy'],
						$_POST['txtConfigVerifiedBy'],
						$_POST['txtConfigCertifiedBy'],
						$filenameAttachment1,
						$filenameAttachment2,				
						$filenameAttachment3,
						$attachment1Name,
						$attachment2Name,
						$attachment3Name,
						$_GET['blueprint']
						);
					if ($result){
						echo '<div class="updated fade"><p>Updated blueprint. <br><a href="' . wp_nonce_url("admin.php?page=phbm_manage_blueprints", "phbm-nonce") . '">Click here to manage blueprints</a></p></div>';
						$tableDisplay = "none";
					}else{
						echo '<div class="updated fade"><p>Error: Not added, please check values and try again.</p></div>';
					}
					$tableDisplay = "none";
				} catch (Exception $ex) {
					echo '<div class="updated fade"><p>Error with file upload. ' . $ex->getMessage() . '</p></div>';
				}
			} elseif(isset($_GET['blueprint'])){
				$sqlResults = $this->selectBlueprintbyID($_GET['blueprint']);
				
				$blueprint = '<img src="' . $this->upload_dir . $sqlResults[0]->Blueprint . '">';
				if (strlen($sqlResults[0]->Attachment1)){
					$blueprint .= '<br><a href="' . $this->upload_dir . $sqlResults[0]->Attachment1 . '" target="_blank">Attachment 1. ' . $sqlResults[0]->Attachment1Name . '</a>';
				}
				if (strlen($sqlResults[0]->Attachment2)){
					$blueprint .= '<br><a href="' . $this->upload_dir . $sqlResults[0]->Attachment2 . '" target="_blank">Attachment 2. ' . $sqlResults[0]->Attachment2Name . '</a>';
				}
				if (strlen($sqlResults[0]->Attachment3)){
					$blueprint .= '<br><a href="' . $this->upload_dir . $sqlResults[0]->Attachment3 . '" target="_blank">Attachment 3. ' . $sqlResults[0]->Attachment3Name . '</a>';
				}
				$description = $sqlResults[0]->Description;
				$architect = $sqlResults[0]->Architect;
				$blueprintVerifiedBy = $sqlResults[0]->BlueprintVerifiedBy;
				$blueprintCertifiedBy = $sqlResults[0]->BlueprintCertifiedBy;
				$configVerifiedBy = $sqlResults[0]->ConfigVerifiedBy;
				$configCertifiedBy = $sqlResults[0]->ConfigCertifiedBy;
				$statusID = $sqlResults[0]->Status_FK;
				$industryID = $sqlResults[0]->Industry_FK;
				$categoryID = $sqlResults[0]->Category_FK;
				$reviewID = $sqlResults[0]->Review_FK;
				$revision = $sqlResults[0]->Revision;
				$dateSubmitted = $sqlResults[0]->DateSubmitted;
				$friendlyDateSubmitted = date('m/d/y',strtotime($dateSubmitted));
				$URL = $sqlResults[0]->URL;
				$estHardware = $sqlResults[0]->EstHardware;
				$estSoftware = $sqlResults[0]->EstSoftware;
				$estConfigTime = $sqlResults[0]->EstConfigTime;
				$estConfigCost = $sqlResults[0]->EstConfigCost;
				$attachment1Name = $sqlResults[0]->Attachment1Name;
				$attachment2Name = $sqlResults[0]->Attachment2Name;
				$attachment3Name = $sqlResults[0]->Attachment3Name;
				$notes = $sqlResults[0]->Notes;
				$returnLink = '<a href="' . wp_nonce_url("admin.php?page=phbm_manage_blueprints", "phbm-nonce") . '">Return to Manage Blueprints (discard changes)</a>';
			}
			$statuses = $this->selectStatuses();
			$industries = $this->selectIndustries();
			$categories = $this->selectCategories();
			$reviews = $this->selectReviews();
			//$privacies = $this->selectPrivacy();
			include('ph-blueprint-manager-edit-blueprint.php');
		}
		
		function fileUpload($fileName, $fileSize, $fileError, $fileTmp)
		{
			//explictly declared file extentions
			$allowedExts = array("VSD", "CFG", "TXT", "TAR", "ZIP", "IMG", "ISO", "OVF", "GIF", "TIF", "JPG", "JPEG", "PNG", "IMG", "OVF", "CFG", "DOC", "DOCX", "XLS", "XLSX");
			$temp = explode(".", $fileName);
			$extension = end($temp);
			//test file extension
			if (!in_array(strtoupper($extension), $allowedExts)){
				throw new Exception('The file extention is not allowed: ' . $fileName);
			}
			//test file size
			if ($fileSize > 24000000){
				throw new Exception('The file size exceeds limit or 24MB.');
			}
			//looking for errors generated by php, call to fileErrorCodes for plain text
			if ($fileError > 0){
				throw new Exception($this->fileErrorCodes($fileError));
			}else{	
				//so far so good, check for existing file, if found put a number at the beginning of file name
				$i = "";
				if(file_exists($this->file_move_dir . $fileName)){
					$i = 1;
					while (file_exists($this->file_move_dir . $i . $fileName)){
						$i++;
					}
				}
				//everything checks out, move the file from temp to the uploads directory declared in class variable (folder titles uploads under plugin
				if (move_uploaded_file($fileTmp, $this->file_move_dir . $i . $fileName)){
					return $i . $fileName;
				}else{
					//shit, something didn't work, check existence and permissions of upload folder 
					throw new Exception('The move_uploaded error');
				}
			}	
		}
		
		function fileErrorCodes ($error){
		//this is a translator for the php file tupe error codes
			switch ($error){
				case 1:
					return "The uploaded file exceeds the upload_max_filesize directive in php.ini.";
				case 2:
					return "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.";
				case 3: 
					return "The uploaded file was only partially uploaded.";
				case 4:
					return "No file was uploaded.";
				case 6:
					return "Missing a temporary folder.";
				case 7:
					return "Failed to write file to disk.";
				case 8:
					return "A PHP extension stopped the file upload.";
				default:
					return "";
			}
		}
		
		///**********Select functions for all SQL tables**********///
		function selectRow($table, $id){
			global $wpdb;
			$query="SELECT * FROM " . $wpdb->prefix . "phbm_" . $table . "  
				WHERE id='" . $id . "'";
			return $wpdb->get_results($wpdb->prepare($query, ''));
		}
		
		function selectTable($table){
			global $wpdb;
			$query="SELECT * FROM " . $wpdb->prefix . "phbm_" . $table;
			return $wpdb->get_results($wpdb->prepare($query, ''));
		}
		
		function selectBlueprints(){
			global $wpdb;
			$query="SELECT B.*, R.Description as ReviewDescription, C.Description as CategoryDescription, I.Description as IndustryDescription, P.Description as PrivacyDescription, S.Description as StatusDescription FROM " . $wpdb->prefix . "phbm_blueprints B
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_categories
				) C 
					ON B.Category_FK = C.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_industries
				) I 
					ON B.Industry_FK = I.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_privacy
				) P 
					ON B.Privacy_FK = P.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_statuses
				) S 
					ON B.Status_FK = S.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_reviews
				) R 
					ON B.Review_FK = R.ID
			ORDER BY B.DateSubmitted DESC";
			return $wpdb->get_results($wpdb->prepare($query, ''));
		}
		
		function selectBlueprintsApproved(){
			global $wpdb;
			$query="SELECT B.*, R.Description as ReviewDescription, C.Description as CategoryDescription, I.Description as IndustryDescription, P.Description as PrivacyDescription, S.Description as StatusDescription FROM " . $wpdb->prefix . "phbm_blueprints B
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_categories
				) C 
					ON B.Category_FK = C.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_industries
				) I 
					ON B.Industry_FK = I.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_privacy
				) P 
					ON B.Privacy_FK = P.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_statuses
				) S 
					ON B.Status_FK = S.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_reviews
				) R 
					ON B.Review_FK = R.ID
			WHERE B.Review_FK = 3";
			return $wpdb->get_results($wpdb->prepare($query, ''));
		}
		
		function selectRecentBlueprint(){
			global $wpdb;
			$query="SELECT B.*, R.Description as ReviewDescription, C.Description as CategoryDescription, I.Description as IndustryDescription, P.Description as PrivacyDescription, S.Description as StatusDescription FROM " . $wpdb->prefix . "phbm_blueprints B
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_categories
				) C 
					ON B.Category_FK = C.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_industries
				) I 
					ON B.Industry_FK = I.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_privacy
				) P 
					ON B.Privacy_FK = P.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_statuses
				) S 
					ON B.Status_FK = S.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_reviews
				) R 
					ON B.Review_FK = R.ID
			WHERE B.Review_FK = 3
			ORDER BY B.DateSubmitted DESC
			LIMIT 1";
			return $wpdb->get_results($wpdb->prepare($query, ''));
		}
		
		function selectBlueprintsbyCategoryApproved($category){
			global $wpdb;
			$query="SELECT B.*, R.Description as ReviewDescription, I.Description as IndustryDescription, P.Description as PrivacyDescription, S.Description as StatusDescription FROM " . $wpdb->prefix . "phbm_blueprints B
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_industries
				) I 
					ON B.Industry_FK = I.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_privacy
				) P 
					ON B.Privacy_FK = P.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_statuses
				) S 
					ON B.Status_FK = S.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_reviews
				) R 
					ON B.Review_FK = R.ID
			WHERE Category_FK = " . $category . " AND B.Review_FK = 3";
			return $wpdb->get_results($wpdb->prepare($query, ''));
		}
		
		function selectBlueprintbyID($id){
			global $wpdb;
			$query="SELECT B.*, R.Description as ReviewDescription, C.ID as CategoryID, C.Description as CategoryDescription, I.ID as IndustryID, I.Description as IndustryDescription, P.ID as PrivacyID, P.Description as PrivacyDescription, S.ID as StatusID, S.Description as StatusDescription FROM " . $wpdb->prefix . "phbm_blueprints B
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_categories
				) C 
					ON B.Category_FK = C.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_industries
				) I 
					ON B.Industry_FK = I.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_privacy
				) P 
					ON B.Privacy_FK = P.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_statuses
				) S 
					ON B.Status_FK = S.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_reviews
				) R 
					ON B.Review_FK = R.ID
			WHERE B.ID = " . $id;
			return $wpdb->get_results($wpdb->prepare($query, ''));
		}
		
		function selectBluePrintsTopRated($limit = null) {
		
			global $wpdb;
			$query="SELECT B.*, R.Description as ReviewDescription, C.Description as CategoryDescription, I.Description as IndustryDescription, P.Description as PrivacyDescription, S.Description as StatusDescription, COALESCE( RA.AvgRating, 0) as Rating FROM " . $wpdb->prefix . "phbm_blueprints B
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_categories
				) C 
					ON B.Category_FK = C.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_industries
				) I 
					ON B.Industry_FK = I.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_privacy
				) P 
					ON B.Privacy_FK = P.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_statuses
				) S 
					ON B.Status_FK = S.ID
			LEFT JOIN (SELECT ID, Description FROM " . $wpdb->prefix . "phbm_reviews
				) R 
					ON B.Review_FK = R.ID
			LEFT JOIN (SELECT AVG(rating) AS AvgRating, BluePrint_FK FROM " . $wpdb->prefix . "phbm_ratings Group By BluePrint_FK) RA ON RA.BluePrint_FK = B.ID
			WHERE B.Review_FK = 3
			Order By Rating DESC";
			if($limit != null){
				$query .= " LIMIT ". $limit;
			}
			
			return $wpdb->get_results($wpdb->prepare($query, ''));
		
		
		}
		
		function selectCategories(){
			global $wpdb;
			$query="SELECT * FROM " . $wpdb->prefix . "phbm_categories
			ORDER BY RowOrder ASC";
			return $wpdb->get_results($wpdb->prepare($query, ''));
		}
		
		function selectIndustries(){
			global $wpdb;
			$query="SELECT * FROM " . $wpdb->prefix . "phbm_industries
			ORDER BY RowOrder ASC";
			return $wpdb->get_results($wpdb->prepare($query, ''));
		}
		
		function selectPrivacy(){
			global $wpdb;
			$query="SELECT * FROM " . $wpdb->prefix . "phbm_privacy
			ORDER BY RowOrder ASC";
			return $wpdb->get_results($wpdb->prepare($query, ''));
		}
		
		function selectStatuses(){
			global $wpdb;
			$query="SELECT * FROM " . $wpdb->prefix . "phbm_statuses
			ORDER BY RowOrder ASC";
			return $wpdb->get_results($wpdb->prepare($query, ''));
		}
		
		function selectReviews(){
			global $wpdb;
			$query="SELECT * FROM " . $wpdb->prefix . "phbm_reviews
			ORDER BY RowOrder ASC";
			return $wpdb->get_results($wpdb->prepare($query, ''));
		}
		
		function selectRatingforBlueprint($blueprint){
			global $wpdb;
			$query="SELECT AVG(rating) AS AvgRating FROM " . $wpdb->prefix . "phbm_ratings
			WHERE Blueprint_FK = " . $blueprint;
			return $wpdb->get_results($wpdb->prepare($query, ''));
		}
		
		///**********Delete functions for all SQL tables**********///
		function deleteRow($table, $id){
			global $wpdb;
			$sql = "DELETE FROM ". $wpdb->prefix . "phbm_" . $table . 
				" WHERE id = '%d'";
			$wpdb->query($wpdb->prepare($sql, $id));
		}
		
		///**********Update functions for all SQL tables**********///	
		function updateBlueprint($blueprint, $architect, $URL, $dateSubmitted, $description, $notes, $revision, $status, $industry, $privacy, $category, $review, $estHardware, $estSoftware, $estConfigCost, $estConfigTime, $blueprintVerifiedBy, $blueprintCertifiedBy, $configVerifiedBy, $configCertifiedBy, $attachment1, $attachment2, $attachment3, $attachment1name, $attachment2name, $attachment3name, $id){
			global $wpdb;
			$sql = "UPDATE ". $wpdb->prefix . "phbm_blueprints
				SET Blueprint='%s', Architect='%s', URL='%s', DateSubmitted='%s', Description='%s', Notes='%s', Revision='%s', Status_FK='%d', Industry_FK='%d', Privacy_FK='%d', Category_FK='%d', Review_FK='%d', EstHardware='%s', EstSoftware='%s', EstConfigCost='%s', EstConfigTime='%s', BlueprintVerifiedBy='%s', BlueprintCertifiedBy='%s', ConfigVerifiedBy='%s', ConfigCertifiedBy='%s', Attachment1='%s', Attachment2='%s', Attachment3='%s', Attachment1Name='%s', Attachment2Name='%s', Attachment3Name='%s'
				WHERE id = '%d'";
			$wpdb->query($wpdb->prepare($sql, $blueprint, $architect, $URL, $dateSubmitted, $description, $notes, $revision, $status, $industry, $privacy, $category, $review, $estHardware, $estSoftware, $estConfigCost, $estConfigTime, $blueprintVerifiedBy, $blueprintCertifiedBy, $configVerifiedBy, $configCertifiedBy, $attachment1, $attachment2, $attachment3, $attachment1name, $attachment2name, $attachment3name, $id));
			return $id;
		}
		
		function updateBlueprintPost($postID, $id){
			global $wpdb;
			$sql = "UPDATE ". $wpdb->prefix . "phbm_blueprints
				SET Post_FK='%d'
				WHERE id = '%d'";
			$wpdb->query($wpdb->prepare($sql, $postID, $id));
			return $id;
		}
		
		///**********insert functions for all SQL tables**********///
		function insertBlueprint($bluprintFullImage, $architect, $URL, $dateSubmitted, $description, $notes, $revision, $status, $industry, $privacy, $category, $review, $estHardware, $estSoftware, $estConfigCost, $estConfigTime, $blueprintVerifiedBy, $blueprintCertifiedBy, $configVerifiedBy, $configCertifiedBy, $attachment1, $attachment2, $attachment3, $attachment1name, $attachment2name, $attachment3name, $userID){
			global $wpdb;			
			$wpdb->insert($wpdb->prefix . "phbm_blueprints", array('Blueprint' => $bluprintFullImage, 'Architect' => $architect, 'URL' => $URL, 'DateSubmitted' => $dateSubmitted, 'Description' => $description, 'Notes' => $notes, 'Revision' => $revision, 'Status_FK' => $status, 'Industry_FK' => $industry, 
			'Privacy_FK' => $privacy, 'Category_FK' => $category, 'Review_FK' => $review, 'EstHardware' => $estHardware,'EstSoftware' => $estSoftware, 'EstConfigCost' => $estConfigCost, 'EstConfigTime' => $estConfigTime, 'BlueprintVerifiedBy' => $blueprintVerifiedBy, 'BlueprintCertifiedBy' => $blueprintCertifiedBy, 'ConfigVerifiedBy' => $configVerifiedBy, 'ConfigCertifiedBy' => $configCertifiedBy,
			'Attachment1' => $attachment1, 'Attachment2' => $attachment2, 'Attachment3' => $attachment3, 'Attachment1Name' => $attachment1name, 'Attachment2Name' => $attachment2name, 'Attachment3Name' => $attachment3name, 'User_FK' => $userID), 
			array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%d', '%d', '%d', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s'));
			return $wpdb->insert_id;
		}
		
		function insertRating($blueprint, $rating, $IP){
			global $wpdb;
			$wpdb->insert($wpdb->prefix . "phbm_ratings", array('Blueprint_FK' => $blueprint, 'Rating' => $rating, 'IP' => $IP), array('%d', '%f', '%s'));
			return $wpdb->insert_id;
		}
		
	}
	
else:
	exit("Class Blueprint Manager already declared!");
endif;

$BlueprintManager = new BlueprintManager();

if (isset($BlueprintManager)){
	//register the activation function by passing the reference to our instance
	register_activation_hook(__FILE__, array(&$BlueprintManager,'activate'));
	register_deactivation_hook(__FILE__, array(&$BlueprintManager,'deactivate'));
	register_uninstall_hook(__FILE__, array(&$BlueprintManager,'uninstall'));
}

if ( !function_exists( 'phbm_Display' ) ) {
    function phbm_Display($params) {
        $BlueprintManager = new BlueprintManager();
		echo $BlueprintManager->displayBlueprintMenu();
    }
}
?>