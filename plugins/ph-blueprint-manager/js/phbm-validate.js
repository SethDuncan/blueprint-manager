function phValidateForm(form){

	var isValid = true;


	(jQuery)("input:visible, textarea, select", form).removeClass("invalid");

	///Check Required Elements
	(jQuery)("input:visible, textarea, select", form).each(function () {

		//// Checks Requireds
		if ((jQuery)(this).hasClass("validate-req")){

			//// If Textbox check val or Textarea
			if((jQuery)(this).prop("type") == "text" || (jQuery)(this).prop("type") == "textarea" || (jQuery)(this).prop("type") == "password")
			{

				if((jQuery)(this).val() == ""){
					(jQuery)(this).addClass("invalid");
					isValid = false;
				}
				
			
			}
			else if ((jQuery)(this).is("select"))
			{
				if((jQuery)(this).find("option:selected").val() == "-1")
				{
					(jQuery)(this).addClass("invalid");
					isValid = false;
				}
				
			}
			else if ((jQuery)(this).is("input:radio")){

				var radioName = (jQuery)(this).prop("name");

				if (!((jQuery)("input[name=" + radioName + "]:checked").length >0)){

					isValid = false;
					(jQuery)("input[name=" + radioName + "]").addClass("invalid");
					
				}
				

			}
		}


		/////Check Compares

		if((jQuery)(this).hasClass("validate-comp")){

			if((jQuery)(this).prop("type") == "text" || (jQuery)(this).prop("type") == "password"){

				var that = (jQuery)((jQuery)(this).attr("data-compare"));
				
				

				if((jQuery)(this).val() != (jQuery)(that).val()) {
					isValid = false;
					(jQuery)(this).addClass("invalid");
					(jQuery)(that).addClass("invalid");

					if(jQuery().popover){
						(jQuery)(this).popover({"content": "The fields must match", "placement": "bottom"}).popover('toggle');
						(jQuery)(that).popover({"content": "The fields must match", "placement": "bottom"}).popover('toggle');
					}
				}

				

			}


		}


		///// Check TimeRange
		if((jQuery)(this).hasClass("validate-timerange")){

			var x = (jQuery)(this).val();
			var tgt = (jQuery)(this).attr("data-target");
			var y = (jQuery)(tgt).val();

			var d1 = new Date();
			var time = x.split(" ");

			var hr = (parseInt(time[0].split(":")[0]) == 12) ? 0: time[0].split(":")[0];
			var min = time[0].split(":")[1];
			var adder = (time[1] == "pm") ?12:0;

			hr = parseInt(hr) + parseInt(adder);

			d1.setHours( parseInt(hr));
			d1.setMinutes( parseInt(min) );

			var d2 = new Date();

			time = y.split(" ");

			hr = (parseInt(time[0].split(":")[0]) == 12) ? 0: time[0].split(":")[0];
			min = time[0].split(":")[1];
			adder = (time[1] == "pm") ?12:0;

			hr = parseInt(hr) + parseInt(adder);

			d2.setHours( parseInt(hr) );
			d2.setMinutes( parseInt( min ));

			x = d1.getTime();
			y = d2.getTime();

			if((jQuery)(this).hasClass("timeStart")){
				if (x > y)
				{
					isValid = false;
					(jQuery)(this).addClass("invalid");
				}
			}
			else
			{
				if (x < y)
				{
					isValid = false;
					(jQuery)(this).addClass("invalid");
				}
			}



		}


		//// Check Numerics
		if((jQuery)(this).hasClass("validate-numeric")){

			if ((jQuery)(this).is("input:text")){
				
				var numVal = (jQuery)(this).val();

				if (/\D/.test(numVal)) 
				{
					isValid = false;
					(jQuery)(this).addClass("invalid");
					(jQuery)(this).popover({"content": "Must use numbers only"}).popover('toggle');
				}

			
			}

		}

		//// Only Applies to Radios & Checkboxes
		if((jQuery)(this).hasClass("validate-agree")){

			if ((jQuery)(this).is("input:radio")){

				var radioName = (jQuery)(this).prop("name");

				if ((jQuery)("input[name=" + radioName + "]:checked").length > 0) ////Make sure checked item even exists
				{
				
					if (!((jQuery)("input[name=" + radioName + "]:checked").val().toLowerCase() == "yes")){

						isValid = false;
						(jQuery)("input[name=" + radioName + "]").addClass("invalid");

							
					}
				}
				else
				{
					isValid = false;
					(jQuery)("input[name=" + radioName + "]").addClass("invalid");
				}

			}			


		}


	});


	return isValid;

}

function buildInvalidMessageList()
{
	var buildStr = "";

	var requiredCnt = (jQuery)(".validate-req.invalid").length;
	var numericCnt = (jQuery)(".validate-numeric.invalid").length;
	var agreeCnt = (jQuery)(".validate-agree.invalid").length;
	var compareCnt = (jQuery)(".validate-comp.invalid").length;

	if (requiredCnt > 0){
		buildStr += "<li>You are missing required fields: (" + requiredCnt + ") input(s) </li>";
	}

	if(numericCnt > 0){

		buildStr += "<li>You have entered non-numeric values in numeric only fields: (" + numericCnt + ") input(s) </li>";

	}

	if(compareCnt > 0) {

		buildStr += "<li>You have entered values that do not match on confirmation fields: (" + compareCnt + ") input(s)</li>";

	}

	if(agreeCnt > 0) {

		buildStr += "<li>You must answer yes to the disclaimer in order to proceed</li>";

	}


	return buildStr;



}



(jQuery)(document).ready(function () {

	(jQuery)("input:submit").click(function() {

		(jQuery)(this).attr("clicked", "true");

	});


	(jQuery)("form").on("submit", function () {

		var initiator = (jQuery)("input[type=submit][clicked=true]");

		if((jQuery)(initiator).hasClass("noValidate")){
			return true;
		}else{

			var validStatus = phValidateForm((jQuery)(this));

			if ((jQuery)("#validateDialog").length < 1 || typeof jQuery.ui == 'undefined' || validStatus == true)
			{
				return validStatus;
			}
			else /// Show Validation Message Before Submit 
			{
				

				var invalidStr = buildInvalidMessageList();

				(jQuery)("#validateDialog #validateList").html(invalidStr);


				(jQuery)( "#validateDialog" ).dialog({
			      resizable: false,
			      modal: true,
			      width: 520,
			      title: 'Sorry. You need to fix some things.',
			      buttons: {
			        "Close": {
			        	click: function() {
			          		(jQuery)( this ).dialog( "close" );
			          	},
			          	text: 'Close',
			          	'class': 'btn btn-cta'

			        }
			      }

			    });

				
				return validStatus;

			}

			
		}

		

	});



});