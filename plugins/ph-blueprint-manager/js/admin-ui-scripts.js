(jQuery)(document).ready(function () {

	
	(jQuery)(".date-field").datepicker({
	
		dateFormat: "mm/dd/yy"
	
	});
	
	
	(jQuery)(".date-field-other").datepicker({
	
		altFormat: 'yy-mm-dd',
		dateFormat: "mm/dd/yy"
	});
	
	(jQuery)(".date-field-other").each(function () {
	
		var altField = "#" + (jQuery)(this).prop("id") + "Other";
		(jQuery)(this).datepicker('option', 'altField', altField);
	
	});
	
});